<?php include('inc-header.php'); ?>
   
   <!-- Hero Slider Section -->
    <section class="hero">
      <ul id="lightSlider" class="sliderz list-unstyled position-relative">
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-heroslider2.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <h4 class="h3 p-2">MANJEZ RESTAURANT</h4>
            <p class="px-4">
              In the hart of Belgrade, surounded by one of the most
              beautifulparks in the city, lies the best Restaurant that Belgrade
              canoffer.
            </p>
          </div>
        </li>
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-heroslider1.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <h4 class="h3 p-2">MANJEZ RESTAURANT</h4>
            <p class="px-4">
              Best place in Belgrade to spend quality time with Friends and
              Family
            </p>
          </div>
        </li>
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-heroslider3.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <h4 class="h3 p-2">MANJEZ RESTAURANT</h4>
            <p class="px-4">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam,
              in.
            </p>
          </div>
        </li>
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-heroslider4.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <h4 class="h3 p-2">MANJEZ RESTAURANT</h4>
            <p class="px-4">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia
              quibusdam officiis assumenda.
            </p>
          </div>
        </li>
      </ul>
    </section>

    <!-- Afterhero Section -->

    <section class="afterhero py-5">
      <div class="wrapper text-center">
        <h4 class="h3 text-uppercase pb-5 mb-0">
          Belgrade restaurant | <span>Manjež</span>
        </h4>
        <p class="px-5">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas
          consequatur doloribus animi, saepe nemo nostrum libero praesentium,
          dolorem quas obcaecati molestias eaque mollitia quidem blanditiis non
          ex eius, beatae possimus quisquam. Vero sint consequuntur officiis
          neque, placeat quo aspernatur? Deserunt.
        </p>
        <p class="px-5 mb-0">
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Esse minus
          explicabo totam cum, dolore magnam?
        </p>
        <button class="btn btn-lg btn-outline-danger mt-5" style="width:220px">
          MORE
        </button>
      </div>
    </section>

    <!-- Menu Slider Section -->

    <section class="menuslider">
      <ul id="lightSlider2" class="sliderz list-unstyled position-relative">
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-menuslider3.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <h4 class="h3 p-2">MANJEZ RESTAURANT - MENU</h4>
            <p class="px-4">
              In the hart of Belgrade, surounded by one of the most beautiful
              parks in the city, lies the best Restaurant that Belgrade can
              offer.
            </p>
          </div>
        </li>
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-menuslider2.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <div class="menutxt position-relative">
              <h4 class="h3 p-2">RIŽOTO SA ŠUMSKIM PLODOVIMA</h4>
              <p class="px-4">
                Best place in Belgrade to spend quality time with Friends and
                Family
              </p>
              <div class="pricecirc position-absolute bg-danger text-white">
                <h4 class="h1 text-white">
                  650 RSD
                </h4>
              </div>
            </div>
          </div>
        </li>
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-menuslider1.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <div class="menutxt position-relative">
              <h4 class="h3 p-2">PREDJELO MANJEŽ ZA 2 OSOBE</h4>
              <p class="px-4">
                Best place in Belgrade to spend quality time with Friends and
                Family
              </p>
              <div class="pricecirc position-absolute bg-danger text-white">
                <h4 class="h1 text-white">
                  1350 RSD
                </h4>
              </div>
            </div>
          </div>
        </li>
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-menuslider4.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <div class="menutxt position-relative">
              <h4 class="h3 p-2">BIFTEK NA ŽARU</h4>
              <p class="px-4">
                Best place in Belgrade to spend quality time with Friends and
                Family
              </p>
              <div class="pricecirc position-absolute bg-danger text-white">
                <h4 class="h1 text-white">
                  1100 RSD
                </h4>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </section>

    <!-- Menu Section -->

    <section class="rstrmenu py-5">
      <div class="wrapper text-center">
        <h4 class="h3 text-uppercase pb-5 mb-0">
          Belgrade restaurant | <span>Manjež menu</span>
        </h4>
        <!-- STARTERS -->
        <div class="menuholder row">
          <h4 class="text-uppercase w-100 text-center pb-5">Starters</h4>
          <div class="leftside col-12 col-sm-6">
            <!-- Starters -->
            <hr class="my-0" />
            <h4 class="my-3">Let's start</h4>
            <table class="w-100 m-0 mb-4">
              <tr class="w100">
                <td class="w-75 text-left">Starter Manjež (Plate for 2)</td>
                <td class="w-25">1350,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">Steak Tartare</td>
                <td class="w-25">1600,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">Serbian Morsel</td>
                <td class="w-25">450,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Rolled Zucchini with prosciutto and goat cheese
                </td>
                <td class="w-25">350,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Grilled Prosciutto with Mozzarella cheese
                </td>
                <td class="w-25">760,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Chicken Liver Paste with hazelnut and Bolete mushroom
                </td>
                <td class="w-25">390,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Pork Cracklings pate
                </td>
                <td class="w-25">390,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Pork Cracklings
                </td>
                <td class="w-25">350,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Montenegrin "Njegoš" prosciutto
                </td>
                <td class="w-25">760,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Marinated beef prosciutto
                </td>
                <td class="w-25">650,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Cheese from olive oil
                </td>
                <td class="w-25">450,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  "Sjenica Cheese" Local sheep milk cottage cheese
                </td>
                <td class="w-25">390,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Goat Cheese
                </td>
                <td class="w-25">390,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Homemade green cheese
                </td>
                <td class="w-25">290,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Kaymak (Serbian Clotted Cream)
                </td>
                <td class="w-25">390,00 RSD</td>
              </tr>
            </table>

            <!-- And for the spoon -->
            <hr class="my-0" />
            <h4 class="my-3">And for the spoon</h4>
            <table class="w-100 m-0 mb-4">
              <tr class="w100">
                <td class="w-75 text-left">
                  Beef soup with homemade noodles
                </td>
                <td class="w-25">250,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">Veal cream soup</td>
                <td class="w-25">250,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Tomato cream soup
                </td>
                <td class="w-25">250,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Potage
                </td>
                <td class="w-25">250,00 RSD</td>
              </tr>
            </table>
          </div>
          <div class="rightside col-12 col-sm-6">
            <!-- Easy Warm up -->
            <hr class="my-0" />
            <h4 class="my-3">Easy warm up</h4>
            <table class="w-100 m-0 mb-4">
              <tr class="w100">
                <td class="w-75 text-left">
                  Cheef Raša's beef rolls in pumpkin seed sauce
                </td>
                <td class="w-25">390,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">Grilled cheese</td>
                <td class="w-25">450,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Mushrooms filled with "Sjenica cheese"
                </td>
                <td class="w-25">350,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Grilled Mushrooms
                </td>
                <td class="w-25">290,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Risotto with wild-fruits
                </td>
                <td class="w-25">650,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Risotto with rucola and turkey
                </td>
                <td class="w-25">650,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Tagliatelle with beefsteak in red piquant sauce
                </td>
                <td class="w-25">650,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Tagliatelle with chicken and Gorgonzola
                </td>
                <td class="w-25">650,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Deepfired cheese
                </td>
                <td class="w-25">450,00 RSD</td>
              </tr>
            </table>

            <!-- And maybe for the fork -->
            <hr class="my-0" />
            <h4 class="my-3">And maybe for the fork</h4>
            <table class="w-100 m-0 mb-4">
              <tr class="w100">
                <td class="w-75 text-left">
                  Cooked, baked pork knuckle with Mlinci(Pasta Tatters)
                </td>
                <td class="w-25">890,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">Shank of veal in kaymak</td>
                <td class="w-25">690,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Gourmet tripe
                </td>
                <td class="w-25">490,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Cover charge
                </td>
                <td class="w-25">120,00 RSD</td>
              </tr>
            </table>
          </div>
        </div>
        <hr class="bg-secondary my-0" />
        <!-- MAIN COURSES -->
        <div class="menuholder row">
          <h4 class="text-uppercase py-5 w-100 text-center">Main courses</h4>
          <div class="leftside col-12 col-sm-6">
            <hr class="my-0" />
            <h4 class="py-3">Chicken</h4>
            <table class="w-100 m-0 mb-4">
              <tr class="w100">
                <td class="w-75 text-left">
                  Medallions with kaymak and mushrooms
                </td>
                <td class="w-25">10,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Stuffed with hazelnut and mozzarella
                </td>
                <td class="w-25">10,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">Chicken Breasts in kaymak</td>
                <td class="w-25">0,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Grilled
                </td>
                <td class="w-25">0,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Chicken liver bacon wrapped
                </td>
                <td class="w-25">0,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Pan-fried drumstick with rice
                </td>
                <td class="w-25">0,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Crispy chicken drumstick with cheese and prosciutto
                </td>
                <td class="w-25">0,00 RSD</td>
              </tr>
            </table>

            <!-- Turkey -->
            <hr class="my-0" />
            <h4 class="my-3">Turkey</h4>
            <table class="w-100 m-0 mb-4">
              <tr class="w100">
                <td class="w-75 text-left">
                  With Mlinci (Pasta tetters) in sour cream
                </td>
                <td class="w-25">,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Rolled in salmon in Gorgonzola sauce
                </td>
                <td class="w-25">,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Steak in grapes and emmentaler sauce
                </td>
                <td class="w-25">,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Grilled
                </td>
                <td class="w-25">,00 RSD</td>
              </tr>
            </table>

            <hr class="my-0" />
            <h4 class="my-3">Veal</h4>
            <table class="w-100 m-0 mb-4">
              <tr class="w100">
                <td class="w-75 text-left">
                  Cotlet with bolete mushroom sauce
                </td>
                <td class="w-25">0,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  With grilled cheese in sweet chilly sauce
                </td>
                <td class="w-25">0,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Traditinally roasted (under "Sač")
                </td>
                <td class="w-25">0,00 RSD</td>
              </tr>
            </table>
          </div>
          <div class="rightside col-12 col-sm-6">
            <!-- And maybe for the fork -->
            <hr class="my-0" />
            <h4 class="my-3">And maybe for the fork</h4>
            <table class="w-100 m-0 mb-4">
              <tr class="w100">
                <td class="w-75 text-left">
                  Cooked, baked pork knuckle with Mlinci(Pasta Tatters)
                </td>
                <td class="w-25">890,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">Shank of veal in kaymak</td>
                <td class="w-25">690,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Gourmet tripe
                </td>
                <td class="w-25">490,00 RSD</td>
              </tr>
              <tr class="w100">
                <td class="w-75 text-left">
                  Cover charge
                </td>
                <td class="w-25">120,00 RSD</td>
              </tr>
            </table>
          </div>
        </div>
        <hr class="bg-dark-50 my-0" />
        <button class="btn btn-lg btn-outline-danger mt-5" style="width:220px">
          MORE
        </button>
      </div>
    </section>

    <!-- Booking -->

    <?php include('inc-booking.php'); ?>

    <!-- Villa Slider Section -->
    <section class="villaslider">
      <ul id="lightSlider3" class="sliderz list-unstyled position-relative">
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-villaslider1.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <h4 class="h3 p-2">MANJEZ VILLA - HOTEL</h4>
            <p class="px-4">
              In the hart of Belgrade, surounded by one of the most beautiful
              parks in the city, lies the best Restaurant that Belgrade can
              offer.
            </p>
          </div>
        </li>
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-villaslider2.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <div class="menutxt position-relative">
              <h4 class="h3 p-2">
                Night with breakfast in "Manjež standard" room
              </h4>
              <p class="px-4">
                Best place in Belgrade to spend quality time with Friends and
                Family
              </p>
              <div class="pricecirc position-absolute bg-danger text-white">
                <h4 class="h1 text-white">
                  6900 RSD
                </h4>
              </div>
            </div>
          </div>
        </li>
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-villaslider3.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <div class="menutxt position-relative">
              <h4 class="h3 p-2">
                Night with breakfast in "Manjež Duplex" room
              </h4>
              <p class="px-4">
                Best place in Belgrade to spend quality time with Friends and
                Family
              </p>
              <div class="pricecirc position-absolute bg-danger text-white">
                <h4 class="h1 text-white">
                  7900 RSD
                </h4>
              </div>
            </div>
          </div>
        </li>
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-villaslider4.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <div class="menutxt position-relative">
              <h4 class="h3 p-2">Stay over in "Manjez Duplex" room</h4>
              <p class="px-4">
                Best place in Belgrade to spend quality time with Friends and
                Family
              </p>
              <div class="pricecirc position-absolute bg-danger text-white">
                <h4 class="h1 text-white">
                  4700 RSD
                </h4>
              </div>
            </div>
          </div>
        </li>
        <li class="position-relative">
          <img
            src="images/restoran-manjez-beograd-villaslider5.jpg"
            alt="Restaurant Manjez Belgrade"
          />
          <div
            class="onslidetxt d-none d-lg-block text-center position-absolute wrapper"
          >
            <div class="menutxt position-relative">
              <h4 class="h3 p-2">
                Night with breakfast in "Manjež Junior" apartment
              </h4>
              <p class="px-4">
                Best place in Belgrade to spend quality time with Friends and
                Family
              </p>
              <div class="pricecirc position-absolute bg-danger text-white">
                <h4 class="h1 text-white">
                  12600 RSD
                </h4>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </section>

    <!-- Hotel Prices Section -->
    <section class="hotelprices py-5">
      <div class="wrapper">
        <h4 class="h3 text-uppercase text-center pb-5 mb-0">
          Belgrade restaurant Manjež |<span> Hotel</span>
        </h4>
        <h4 class="text-uppercase text-center pb-3">Room pricing</h4>
        <div>
          <h4 class="text-center pb-3">
            Spend a night in our villa with breakfast included in:
          </h4>
          <table class="pricestable50 w-50 mx-auto">
            <tr>
              <td class="w-75 text-left">Room Manjez standard 1/2</td>
              <td class="w-25 text-right">6.900,00 RSD</td>
            </tr>
            <tr>
              <td class="w-75 text-left">Room Manjez standard 2/2</td>
              <td class="w-25 text-right">7.400,00 RSD</td>
            </tr>
            <tr>
              <td class="w-75 text-left">Room Manjez Duplex 1/2</td>
              <td class="w-25 text-right">7.900,00 RSD</td>
            </tr>
            <tr>
              <td class="w-75 text-left">Room Manjez Duplex 2/2</td>
              <td class="w-25 text-right">8.400,00 RSD</td>
            </tr>
            <tr>
              <td class="w-75 text-left">Room “Manjez 7” Junior apartment 1</td>
              <td class="w-25 text-right">2.600,00 RSD</td>
            </tr>
            <tr>
              <td class="w-75 text-left">Room “Manjez 8” Junior apartment 1</td>
              <td class="w-25 text-right">2.600,00 RSD</td>
            </tr>
            <tr>
              <td class="w-75 text-left">Room “Manjez 9” Junior apartment 1</td>
              <td class="w-25 text-right">2.600,00 RSD</td>
            </tr>
          </table>

          <h4 class="text-center py-3">Stay over</h4>
          <table class="pricestable50 w-50 mx-auto">
            <tr>
              <td class="w-75 text-left">“Manjez” duplex</td>
              <td class="w-25 text-right">4.700,00 RSD</td>
            </tr>
            <tr>
              <td class="w-75 text-left">Junior Apartmani</td>
              <td class="w-25 text-right">6.300,00 RSD</td>
            </tr>
          </table>
        </div>
        <div class="w-100 text-center my-3">
          <p>Tax is included in displayed prices 8%.</p>
          <p>
            Prices does not include local taxes ( 128,00 RSD per person).
          </p>
          <p class="font-weight-normal mb-0">
            Possibility of achieving special discounts for special categories of
            guests.
          </p>
        </div>
      </div>
    </section>
    <!-- Location Section -->
    <section class="hploc">
      <a
        href=""
        class="navbar-toggler p-0 m-0"
        data-toggle="collapse"
        data-target="#mapfoot"
        aria-expanded="true"
      >
        <div href="" class="wrapper text-center text-white">
          <h4 class="h2 py-5 m-0">
            <span class="d-none d-lg-inline-block"
              >RESTAURANT MANJEŽ BELGRADE |</span
            >
            LOCATION
          </h4>
          <img src="images/restoran-manjez-beograd-adressicon.svg" alt="" />
        </div>
      </a>
      <div id="mapfoot" class="w-100 my-0 collapse">
        <div class="mapouter">
          <div class="gmap_canvas">
            <iframe
              id="gmap_canvas"
              src="https://maps.google.com/maps?q=restoran%20manjez&t=&z=15&ie=UTF8&iwloc=&output=embed"
              frameborder="0"
              scrolling="no"
              marginheight="0"
              marginwidth="0"
            ></iframe>
          </div>
        </div>
      </div>
    </section>

    <?php include('inc-footer.php'); ?>