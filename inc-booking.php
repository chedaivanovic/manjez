 <!-- Booking -->

 <section class="booking bg-dark text-center">
   <div class="wrapper py-5">
     <div class="reservation-info">
       <img src="images/restoran-manjez-beograd-info.svg" style="width:40px" alt="Reservation info" />
     </div>

     <h3 class="text-white">Book a table</h3>

     <form action="" class="row">
       <div class="input col-12 col-sm-6 col-lg-3">
         <input class="form-control my-3 py-4 bg-dark text-white" type="text" placeholder="Name" />
       </div>
       <div class="input col-12 col-sm-6 col-lg-3">
         <input class="form-control my-3 py-4 bg-dark text-white" type="text" placeholder="Surname" />
       </div>
       <div class="input col-12 col-sm-6 col-lg-3">
         <input class="form-control my-3 py-4 bg-dark text-white" type="text" placeholder="Phone number" />
       </div>
       <div class="input col-12 col-sm-6 col-lg-3">
         <input class="form-control my-3 py-4 bg-dark text-white" type="text" placeholder="E-mail" />
       </div>
       <div class="input col-12 col-sm-6 col-lg-3">
         <input class="form_date form-control my-3 py-4 bg-dark text-white" type="text" placeholder="Date" />
       </div>
       <div class="input col-12 col-sm-6 col-lg-3">
         <input class="form_time form-control my-3 py-4 bg-dark text-white" type="text" placeholder="Time" />
       </div>
       <div class="input form-group col-12 col-sm-6 col-lg-3 mb-0">
         <select class="sctdd bg-dark text-white form-control my-3 h-auto py-2" name="" id="">
           <option value="" disabled selected>Number of persons</option>
           <option value="">1</option>
           <option value="">2</option>
           <option value="">3</option>
           <option value="">4</option>
           <option value="">5</option>
           <option value="">6</option>
           <option value="">7</option>
           <option value="">8</option>
         </select>
       </div>
       <div class="input my-3 col-12 col-sm-6 col-lg-3 text-center  mb-0">
         <input class="form-control btn btn-outline-danger sctdd" type="submit" value="Book" />
       </div>
     </form>

     <a href="tel:+381113621111" class="book-info book-telefon text-white h4 d-inline-block pb-1">+381 11 362 11 11</a>
     <div class="book-info book-vreme text-light h5">Every day 08-01h</div>
   </div>
 </section>