   <!-- Footer -->
   <footer class="bg-dark py-5">
     <div class="wrapper row">
       <div class="col-12 col-md-4 text-center text-md-left pb-4 pb-md-0">
         <ul class="font-weight-light footlinks">
           <li>
             <a href="index.php" class="text-white">Restaurant Manjež Belgrade</a>
           </li>
           <li>
             <a href="aboutus.php" class="text-white">About us</a>
           </li>
           <li>
             <a href="menu.php" class="text-white">Restaurant menu</a>
           </li>
           <li><a href="#makereservation" class="text-white">Reservation</a></li>
           <li>
             <a href="gallery.php" class="text-white">Gallery</a>
           </li>
           <li>
             <a href="contact.php" class="text-white">Contact</a>
           </li>
         </ul>
       </div>
       <div class="col-12 col-md-4 text-center">
         <h4 class="text-light pb-3">Follow us on:</h4>
         <ul class="footsoc pb-2">
           <li class="d-inline-block  text-center">
             <a class="text-white pl-2" href="https://www.facebook.com/manjez1936" target="_blank"><img src="images/restoran-manjez-beograd-facebook.svg" alt="Restaurant Manjez Belgrade" /></a>
           </li>
           <li class="d-inline-block  text-center">
             <a class="text-white pl-2" href="https://foursquare.com/v/restoran-manje%C5%BE/4d3dbe2984d46ea89643095d" target="_blank"><img src="images/restoran-manjez-beograd-foursquare.svg" alt="Restaurant Manjez Belgrade" /></a>
           </li>
           <li class="d-inline-block text-center">
             <a class="text-white pl-2" href="https://www.instagram.com/manjez_kafana_vila/" target="_blank"><img src="images/restoran-manjez-beograd-instagram.svg" alt="Restaurant Manjez Belgrade" /></a>
           </li>
         </ul>
         <p class="text-white text-center footcpr mb-0">
           Copyright by manjez.rs | Restaurant Manjež Belgrade
         </p>
       </div>
       <div class="col-12 col-md-4 text-white py-5 py-sm-0 st77 text-center d-flex align-items-center justify-content-center">
         <a target="_blank" href="https://webdizajn-beograd.com/" class="text-white text-center">
           <img src="images/restoran-manjez-beograd-s77.svg" alt="Restaurant Manjez Belgrade" />
           <h2 class="h5 text-white">
             Design by Studio77
           </h2>
         </a>
       </div>
     </div>
   </footer>

   <?php include('bookingmodal.php'); ?>
   <script src="js/popper.min.js"></script>
   <script src="js/jquery-3.4.1.min.js"></script>
   <script src="js/bootstrap.min.js"></script>
   <script src="js/javascript.js"></script>
   <script src="js/lightslider.min.js"></script>
   <script src="js/remodal.min.js"></script>

   </body>

   </html>