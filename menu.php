<?php include('inc-header.php') ?>

<!-- Menu Hero Image -->
<section class="menuhero position-relative">
    <img src="images/restoran-manjez-beograd-menuhero.jpg" alt="">
    <div class="menuimgoverlay">
        <div class="menuovertext d-flex align-items-center h-100">
            <h4 class="h2 text-white text-uppercase m-0"><span class="d-none d-md-inline">Belgrade Restaurant Manjez | </span>Menu</h4>
        </div>
    </div>
</section>

<!-- Menu -->
<div class="menuitems">
    <div class="wrapper">

        <div class="menubreakfast">
            <h4 class="m-0 py-5 text-center">BREAKFAST</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">OMELETE (3eggs) <br> <span class="pl-3 text-lowercase d-inline-block">(CHEESE,HAM,BACON OR VEGETABLES)</span></td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">DOMESTIC POLENTA <br> <span class="pl-3 text-lowercase d-inline-block"> WITH CHEESE, KAYMAK AND BACON</span></td>
                    <td class="text-right colorred w-25">290RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">CHEESE PIE WITH YOGURT <br> <span class="pl-3 text-lowercase d-inline-block">PIECE/250 GR</span></td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">FRITTER AND YOGURT <br> <span class="pl-3 text-lowercase d-inline-block">WITH KAYMAK AND CHEESE OR JAM</span></td>
                    <td class="text-right colorred w-25">290RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">BREAKFAST “MANJEŽ” <br> <span class="pl-3 text-lowercase d-inline-block">(EGGS, CHEESE, KAYMAK, URNEBES, BACON, SAUSAGES)</span></td>
                    <td class="text-right colorred w-25">350RSD</td>
                </tr>
            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>

        <div class="menucoldappetizers">
            <h4 class="m-0 py-5 text-center">COLD APPETIZERS</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">STEAK TARTARE (300gr)</td>
                    <td class="text-right colorred w-25">1800RSD</td>

                </tr>
                <tr>
                    <td class="text-left w-75">BELI MRS <br> <span class="pl-3 text-lowercase d-inline-block">(FRESH CHEESE, OLD CHEESE, KAYMAK, GOUDA/CHEDDAR, PAPRIKA IN SOUR CREAM, URNEBES)</span></td>
                    <td class="text-right colorred w-25">750RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">CHEESE PLATE 200gr
                        <br> <span class="pl-3 text-lowercase d-inline-block">(FRESH CHEESE, OLD CHEESE, SJENIČKI CHEESE, SHEEP’S MILK CHEESE, GOUDA/CHEDDAR, SMOKED CHEESE)</span>
                    </td>
                    <td class="text-right colorred w-25">890RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">ČVARCI, CHEESE, KAYMAK, AJVAR (200gr)</td>
                    <td class="text-right colorred w-25">700RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">NJEGUŠKI PROSCIUTTO, KULEN, CHEESE, OLIVES</td>
                    <td class="text-right colorred w-25">950RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">PLATE “MANJEŽ” 600gr <br> <span class="pl-3 text-lowercase d-inline-block">(ČVARCI, BEEF PROSCIUTTO, KAYMAK, KULEN SAUSAGE, NJEGUŠKI PROSCIUTTO, PAPRIKA IN SOUR CREAM, FRESH CHEESE, OLD CHEESE, KIDNEY BEAN, URNEBES,BACON)</span></td>
                    <td class="text-right colorred w-25">1600RSD</td>
                </tr>
            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>


        <div class="menuhotappetizers">
            <h4 class="m-0 py-5 text-center">HOT APPETIZERS</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">BREADED PAPRIKA STUFFED WITH CHEESE (200gr)</td>
                    <td class="text-right colorred w-25">420RSD</td>

                </tr>
                <tr>
                    <td class="text-left w-75">GRILLED MUSHROOMS (200gr)</td>
                    <td class="text-right colorred w-25">450RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">RISOTTO WITH VEGETABLES
                    </td>
                    <td class="text-right colorred w-25">690RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">RISOTTO WITH CHICKEN</td>
                    <td class="text-right colorred w-25">890RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">RISOTTO WITH STEAK</td>
                    <td class="text-right colorred w-25">990RSD</td>
                </tr>

            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>

        <div class="menusoups">
            <h4 class="m-0 py-5 text-center">SOUPS, BROTHS AND POTAGES</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">SOUP OF THE DAY
                    </td>
                    <td class="text-right colorred w-25">200RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">VEAL BROTH</td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">POTAGE OF THE DAY</td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>

        <div class="menumealsalads">
            <h4 class="m-0 py-5 text-center">SALADS AS A MEAL</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">CAESAR SALAD
                    </td>
                    <td class="text-right colorred w-25">750RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">STEAK SALAD</td>
                    <td class="text-right colorred w-25">950RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">TURKEY SALAD</td>
                    <td class="text-right colorred w-25">750RSD</td>
                </tr>
            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>

        <div class="menufish">
            <h4 class="m-0 py-5 text-center">FISH DISHES</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">SALMON STEAK WITH VEGETABLES (250gr)
                    </td>
                    <td class="text-right colorred w-25">1200RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">GRILLED SMOKED TROUT FILLET WITH VEGETABLES (250gr)</td>
                    <td class="text-right colorred w-25">970RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">GRILLED CALAMARIS WITH VEGETABLES (250gr)</td>
                    <td class="text-right colorred w-25">1190RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">CAPTAIN CALAMARIS (250gr)</td>
                    <td class="text-right colorred w-25">1190RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">CAPTAIN STEW ON POLENTA (250gr)</td>
                    <td class="text-right colorred w-25">1250RSD</td>
                </tr>
            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>

        <div class="menuspecial">
            <h4 class="m-0 py-5 text-center">SPECIAL ORDERS</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">GRILLED CHICKEN BREAST (300gr)
                    </td>
                    <td class="text-right colorred w-25">650RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">CHICKEN IN FOUR CHEESE SAUCE (300gr)</td>
                    <td class="text-right colorred w-25">890RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">“KARAĐORĐEVA” CHICKEN STEAK (300gr)</td>
                    <td class="text-right colorred w-25">840RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">CHICKEN SOTE „MANJEŽ“ (300gr) <br><span class="pl-3 text-lowercase d-inline-block">(CHICKEN WITH VEGETABLES AND WHITE WINE IN AROMATIC RED SAUCE)</span></td>
                    <td class="text-right colorred w-25">950RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">GRILLED TURKEY WITH BOILED VEGETABLES (300gr)</td>
                    <td class="text-right colorred w-25">790RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">TURKEY WITH “MLINCI” (400gr)</td>
                    <td class="text-right colorred w-25">990RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">TURKEY IN MUSHROOMS SAUCE (300gr)</td>
                    <td class="text-right colorred w-25">990RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">“KARAĐORĐEVA” STEAK (300gr)</td>
                    <td class="text-right colorred w-25">890RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">“A LA VIENNESE” STEAK (300gr)</td>
                    <td class="text-right colorred w-25">750RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">MUĆKALICA (300gr)</td>
                    <td class="text-right colorred w-25">990RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">GRILLED PORK FILLET WITH KAYMAK (300gr)</td>
                    <td class="text-right colorred w-25">890RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">STEAK „TALJATA“ (300gr) <br><span class="pl-3 text-lowercase d-inline-block">(BEEF TENDERLOIN STEAK, PARMESAN,RUCOLA)</span></td>
                    <td class="text-right colorred w-25">1800RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">GRILLED STEAK WITH GRILLED VEGETABLES (300gr)</td>
                    <td class="text-right colorred w-25">1650RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">STEAK IN MUSHROOMS SAUCE (300gr)</td>
                    <td class="text-right colorred w-25">1750RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">STEAK “MANJEŽ” (300gr) <br><span class="pl-3 text-lowercase d-inline-block">(PIECES OF STEAK IN TOMATO SAUCE,RED WINE,ONION AND CHILI; GRILLED VEGETABLES AND POTATO WITH AROMATIC HERBS)</span></td>
                    <td class="text-right colorred w-25">1900RSD</td>
                </tr>
            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>

        <div class="menugrilled">
            <h4 class="m-0 py-5 text-center">GRILLED DISHES</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">ĆEVAPČIĆI (300gr)
                    </td>
                    <td class="text-right colorred w-25">690RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">ĆEVAPČIĆI WITH KAYMAK (300gr)</td>
                    <td class="text-right colorred w-25">730RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">PLJESKAVICA (300gr)</td>
                    <td class="text-right colorred w-25">690RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">GOURMENT PLJESKAVICA (300gr)</td>
                    <td class="text-right colorred w-25">740RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">PLJESKAVICA WITH KAYMAK (300gr)</td>
                    <td class="text-right colorred w-25">730RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">PORK COLLAR STEAK (300gr)</td>
                    <td class="text-right colorred w-25">690RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">CHICKEN DRUMSTICK (300gr)</td>
                    <td class="text-right colorred w-25">690RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">HOMEMADE SAUSAGES (300gr)</td>
                    <td class="text-right colorred w-25">690RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">SLICED PORK LOIN (300gr)</td>
                    <td class="text-right colorred w-25">790RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">SMOKED PORK COLLAR STEAK (300gr)</td>
                    <td class="text-right colorred w-25">860RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">SMOKED SLICED PORK LOIN (300gr)</td>
                    <td class="text-right colorred w-25">860RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">SMOKED CHICKEN DRUMSTICK (300gr)</td>
                    <td class="text-right colorred w-25">770RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">RAŽNJIĆI (300gr)</td>
                    <td class="text-right colorred w-25">770RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">ĆEVAPČIĆI „MANJEŽ“ (300gr)</td>
                    <td class="text-right colorred w-25">770RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">MIXED GRILL “MANJEŽ” (400gr) <br><span class="pl-3 text-lowercase d-inline-block">(ĆEVAPI, CHICKEN DRUMSTICK,PORK COLLAR STEAK,GRILLED SAUSAGE,BACON,PLJESKAVICA)</span></td>
                    <td class="text-right colorred w-25">950RSD</td>
                </tr>
                <tr>
                    <td class="text-center w-100 font-weight-normal">*WITH GRILLED DISHES WE SERVE SPICY POTATOES OR FRENCH FRIES </td>
                </tr>
            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>

        <div class="menuhousespecial">
            <h4 class="m-0 py-5 text-center">SPECIALTY OF THE HOUSE</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">BEEF UNDER THE BAKING BEL (1kg)
                    </td>
                    <td class="text-right colorred w-25">3200RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">SMOKED PORK RIBS WITH KAYMAK (400gr)</td>
                    <td class="text-right colorred w-25">1300RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">CABAGE WITH SMOKED PORK KNUCKLES (1kg)</td>
                    <td class="text-right colorred w-25">1600RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">SMOKED HAM HOCK UNDER THE BAKING BEL (1kg)</td>
                    <td class="text-right colorred w-25">1800RSD</td>
                </tr>
            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>

        <div class="menusauces">
            <h4 class="m-0 py-5 text-center">SAUCES</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">GORGONZOLA SAUCE
                    </td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">PEPPER SAUCE</td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">MUSHROOM SAUCE</td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">FOUR CHEESE SAUCE</td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>

        <div class="menusalads">
            <h4 class="m-0 py-5 text-center">SALADS</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">“ŠOPSKA” SALAD
                    </td>
                    <td class="text-right colorred w-25">290RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">VITAMIN SALAD</td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">TOMATO SALAD</td>
                    <td class="text-right colorred w-25">220RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">CABAGE SALAD</td>
                    <td class="text-right colorred w-25">180RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">GRILLED SWEET PAPRIKA
                    </td>
                    <td class="text-right colorred w-25">240RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">GREEN MIXTURE SALAD</td>
                    <td class="text-right colorred w-25">320RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">GARDEN SALAD</td>
                    <td class="text-right colorred w-25">450RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">CHILLI (piece)</td>
                    <td class="text-right colorred w-25">90RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">TARATOR SALAD
                    </td>
                    <td class="text-right colorred w-25">290RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">SALAD “MANJEŽ”</td>
                    <td class="text-right colorred w-25">350RSD</td>
                </tr>
            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>

        <div class="menubreads">
            <h4 class="m-0 py-5 text-center">BREADS</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">BREAD FROM OUR BAKERY
                    </td>
                    <td class="text-right colorred w-25">145RSD</td>
                </tr>
            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>

        <div class="menudeserts">
            <h4 class="m-0 py-5 text-center">DESERTS</h4>
            <table class="w-75 my-0 mx-auto p-0">
                <tr>
                    <td class="text-left w-75">CHOCOLATE TART
                    </td>
                    <td class="text-right colorred w-25">280RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">DOMESTIC PIES <br><span class="pl-3 text-lowercase d-inline-block">(APPLE,CHERRY,DRIED PLUMS)</span></td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">CREPES</td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">PIE WITH WALNUTS</td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">DUMPLINGS WITH PLUMS</td>
                    <td class="text-right colorred w-25">250RSD</td>
                </tr>
                <tr>
                    <td class="text-left w-75">ICE CREAM</td>
                    <td class="text-right colorred w-25">60RSD</td>
                </tr>
            </table>
            <hr class="p-0 mt-5 bg-dark-50 w-75">
        </div>
    </div>
</div>

<?php include('inc-footer.php') ?>