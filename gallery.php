<?php include('inc-header.php') ?>

<!-- Gallery -->

<section class="galleryhero position-relative">
    <img src="images/restoran-manjez-beograd-galleryhero.jpg" class="w-100" alt="">
    <div class="menuimgoverlay">
        <div class="menuovertext d-flex align-items-center h-100">
            <h4 class="h2 text-white text-uppercase m-0"><span class="d-none d-md-inline">Belgrade Restaurant Manjez | </span>Gallery</h4>
        </div>
    </div>
</section>

<?php include('inc-footer.php') ?>