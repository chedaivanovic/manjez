<!DOCTYPE html>
<html lang="sr">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <title>Restoran Manjež | Beograd</title>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Playfair+Display:400,700&amp;subset=latin-ext" rel="stylesheet" />
  <!-- Resources -->
  <link rel="stylesheet" href="../css/lightslider.min.css" />
  <link rel="stylesheet" href="../css/bootstrap.min.css" />
  <link rel="stylesheet" href="../css/style.css" />
  <script src="../js/jquery-3.4.1.min.js"></script>


  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="57x57" href="../images/favicon/apple-icon-57x57.png" />
  <link rel="apple-touch-icon" sizes="60x60" href="../images/favicon/apple-icon-60x60.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="../images/favicon/apple-icon-72x72.png" />
  <link rel="apple-touch-icon" sizes="76x76" href="../images/favicon/apple-icon-76x76.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="../images/favicon/apple-icon-114x114.png" />
  <link rel="apple-touch-icon" sizes="120x120" href="../images/favicon/apple-icon-120x120.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="../images/favicon/apple-icon-144x144.png" />
  <link rel="apple-touch-icon" sizes="152x152" href="../images/favicon/apple-icon-152x152.png" />
  <link rel="apple-touch-icon" sizes="180x180" href="../images/favicon/apple-icon-180x180.png" />
  <link rel="icon" type="image/png" sizes="192x192" href="../images/favicon/android-icon-192x192.png" />
  <link rel="icon" type="image/png" sizes="32x32" href="../images/favicon/favicon-32x32.png" />
  <link rel="icon" type="image/png" sizes="96x96" href="../images/favicon/favicon-96x96.png" />
  <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon/favicon-16x16.png" />
  <link rel="manifest" href="../images/favicon/manifest.json" />
  <meta name="msapplication-TileColor" content="#ffffff" />
  <meta name="msapplication-TileImage" content="../images/favicon/ms-icon-144x144.png" />
  <meta name="theme-color" content="#ffffff" />

  <!-- Open Graph -->

  <meta property="og:title" content="Restoran Manjež Beograd" />
  <meta property="og:site_name" content="Restoran Manjež Beograd" />
  <meta property="og:url" content="https://manjez.rs" />
  <meta property="og:description" content="Restoran Manjež je jedan od najstarijih tradicionalnih restorana u Beogradu. Odlična kuhinja,odlična usluga i sjajan hotel po niskim cenama." />
  <meta property="og:type" content="restaurant" />
  <meta property="og:image" content="https://manjez.rs/images/restoran-manjez-belgrade-logo.png" />
</head>

<body>
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Restaurant",
      "name": "Restoran Manjež Beograd",
      "image": "https://manjez.rs/images/restoran-manjez-beograd-logo.png",
      "@id": "",
      "url": "https://manjez.rs",
      "telephone": "+ 381 11 362 11 11",
      "priceRange": "$",
      "menu": "https://manjez.rs/menu",
      "servesCuisine": "International",
      "acceptsReservations": "true",
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "Svetozara Markovića 49",
        "addressLocality": "Belgrade",
        "postalCode": "11000",
        "addressCountry": "RS"
      },
      "geo": {
        "@type": "GeoCoordinates",
        "latitude": 44.80419864601798,
        "longitude": 20.464742481708527
      },
      "openingHoursSpecification": {
        "@type": "OpeningHoursSpecification",
        "dayOfWeek": [
          "Monday",
          "Tuesday",
          "Wednesday",
          "Thursday",
          "Friday",
          "Saturday",
          "Sunday"
        ],
        "opens": "08:00",
        "closes": "01:00"
      },
      "sameAs": [
        "https://www.instagram.com/manjez_kafana_vila/",
        "https://twitter.com/manjezvilla",
        "https://www.facebook.com/manjez1936/"
      ]
    }
    }
  </script>
  <header>
    <div id="uphead" class="heading-100">
      <div class="wrapper text-center text-md-left">
        <h1 class="">
          Restoran Manjez | Beograd | Svetozara Markovića 49
        </h1>
        <ul class="catbtnhead">
          <li class="mr-2">
            <a class="text-white" href="tel:+ 381 11 362 11 11">
              <img src="../images/restoran-manjez-beograd-phoneicon.svg" alt="Restoran Manjež Beograd" />
              <p>+ 381 11 362 11 11</p>
            </a>
          </li>
          <li>
            <a class="text-white" href=""><img src="../images/restoran-manjez-beograd-adressicon.svg" alt="Restoran Manjež Beograd" />
              <p>Svetozara Markovića 49</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div id="mainnavwhole" class="header-100">
      <div class="wrapper">
        <a href="index.php" class="logo"><img id="logonav" src="../images/restoran-manjez-beograd-logo.png" alt="Restoran Manjež Beograd" /></a>

        <nav id="mainnav" class="navbar-expand-md">
          <div class="inmain">
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav" id="maincolbutt">
              <img src="../images/restoran-manjez-beograd-menu.svg" alt="Restoran Manjež Beograd" />
            </button>
            <div class="collapse navbar-collapse text-center" id="navbarNav">
              <ul class="navbar-nav list-unstyled bg-white">
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="index.php"><span>Početna</span></a>
                  </h2>
                </li>
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="aboutus.php"><span>O nama</span></a>
                  </h2>
                </li>
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="menu.php"><span>Meni</span></a>
                  </h2>
                </li>
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="#makereservation"><span>Rezervacija</span></a>
                  </h2>
                </li>
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="gallery.php"><span>Galerija</span></a>
                  </h2>
                </li>
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="contact.php"><span>Kontakt</span></a>
                  </h2>
                </li>
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="../index.php"><span>EN</span></a>
                  </h2>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
  </header>