<?php include('inc-header.php') ?>

<!-- About us -->
<img src="../images/restoran-manjez-beograd-manjez.jpg" alt="" class="w-100">
<section class="aboutus text-center py-5">
  <div class="wrapper">
    <h4 class="h2 text-uppercase text-danger p-0 pb-5 m-0"><span class="text-dark">Belgrade Restaurant |</span> Manjez</h4>
    <h4 class="h5 text-dark p-0 pb-5 m-0">About us</h4>
    <p class="p-0 m-0">Located 50 m from the Slavia Square in the center of Belgrade and surrounded by the greenery of Manjež Park, Manjež Exclusive Villa offers elegant interiors featuring a traditional décor and free Wi-Fi. LCD cable TV, DVD player and mini-bar are standard facilities. <br><br>
      Manjež Lounge & Club offers a variety of alcoholic and nonalcoholic drinks, popular cocktails and quality wines. The Manjez Restaurant features a terrace and serves international and traditional Serbian dishes, with live music played during the evenings. <br> <br>
      All accommodations units are air conditioned and luxuriously appointed. Each has a private bathroom fitted with a bathrobe, slippers and free toiletries. <br> <br>
      The Yugoslav Drama Theater is just steps away from the Manjež Villa. There is a grocery shop 50 m away, tram and bus stops are within 328 feet, and a wellness center is about 200 m away.<br> <br>
      Main bus and train stations are at a distance of 2625 feet, while the nearest access to E75 Motorway is 0.6 mi away.
      Savski Venac is a great choice for travelers interested in sightseeing, city walks and old-town exploration.<br> <br>
      This is our guests' favorite part of Belgrade, according to independent reviews.
      Couples in particular like the location – they rated it 9.2 for a two-person trip.<br> <br>
      <span class="font-weight-bold"> We speak your language!</span> </p>
  </div>
</section>

<section class="aboutusslider">
  <ul id="lightSlider" class="sliderz list-unstyled position-relative">
    <li class="position-relative">
      <img src="../images/restoran-manjez-beograd-history1.jpg" alt="Restaurant Manjez Belgrade" />
      <div class="onslidetxt d-none d-lg-block text-center position-absolute wrapper">
        <h4 class="h3 p-2">MANJEZ RESTAURANT</h4>
        <p class="px-4">
          In the hart of Belgrade, surounded by one of the most
          beautifulparks in the city, lies the best Restaurant that Belgrade
          canoffer.
        </p>
      </div>
    </li>
    <li class="position-relative">
      <img src="../images/restoran-manjez-beograd-history2.jpg" alt="Restaurant Manjez Belgrade" />
      <div class="onslidetxt d-none d-lg-block text-center position-absolute wrapper">
        <h4 class="h3 p-2">MANJEZ RESTAURANT</h4>
        <p class="px-4">
          Best place in Belgrade to spend quality time with Friends and
          Family
        </p>
      </div>
    </li>
    <li class="position-relative">
      <img src="../images/restoran-manjez-beograd-history3.jpg" alt="Restaurant Manjez Belgrade" />
      <div class="onslidetxt d-none d-lg-block text-center position-absolute wrapper">
        <h4 class="h3 p-2">MANJEZ RESTAURANT</h4>
        <p class="px-4">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam,
          in.
        </p>
      </div>
    </li>
    <li class="position-relative">
      <img src="../images/restoran-manjez-beograd-history4.jpg" alt="Restaurant Manjez Belgrade" />
      <div class="onslidetxt d-none d-lg-block text-center position-absolute wrapper">
        <h4 class="h3 p-2">MANJEZ RESTAURANT</h4>
        <p class="px-4">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia
          quibusdam officiis assumenda.
        </p>
      </div>
    </li>
    <li class="position-relative">
      <img src="../images/restoran-manjez-beograd-history5.jpg" alt="Restaurant Manjez Belgrade" />
      <div class="onslidetxt d-none d-lg-block text-center position-absolute wrapper">
        <h4 class="h3 p-2">MANJEZ RESTAURANT</h4>
        <p class="px-4">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia
          quibusdam officiis assumenda.
        </p>
      </div>
    </li>
  </ul>
</section>
<section class="aboutushistory text-center py-5">
  <div class="wrapper">
    <h4 class="h2 text-uppercase text-danger p-0 pb-5 m-0"><span class="text-dark">Belgrade Restaurant |</span> Manjez</h4>
    <h4 class="h5 text-dark p-0 pb-5 m-0">History</h4>
    <p>The Belgrade City General Ordinance Plan of 1923 envisioned the creation of a park on the location of the Royal Cavalry Guard, or manjež (from the French manège), which later gave name to the park. The Royal Cavalry Guard occupied the site until 1931 when the construction of the park began. It was finished by 1933. The designer was Aleksandar Krstić, a pioneer of modern landscape architecture. The park is one of the few green areas within the city, built between the World wars in the classical style. It was originally named "His Majesty, Heir Apparent Peter.</p>
  </div>
</section>



<?php include('inc-footer.php') ?>