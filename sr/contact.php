<?php include('inc-header.php') ?>

<!-- Contact -->

<!-- Map -->

<section id="contactmap">
    <div class="mapouter">
        <div class="gmap_canvas">
            <div class="mapouter">
                <div class="gmap_canvas">
                    <div id="gmap_canvas"></div>
                    <script>
                        function initMap() {
                            var location = {
                                lat: 44.804199,
                                lng: 20.464744
                            };
                            var map = new google.maps.Map(
                                document.getElementById("gmap_canvas"), {
                                    zoom: 18,
                                    center: location,
                                    styles: [{
                                            "elementType": "geometry",
                                            "stylers": [{
                                                "color": "#ebe3cd"
                                            }]
                                        },
                                        {
                                            "elementType": "labels.text.fill",
                                            "stylers": [{
                                                "color": "#523735"
                                            }]
                                        },
                                        {
                                            "elementType": "labels.text.stroke",
                                            "stylers": [{
                                                "color": "#f5f1e6"
                                            }]
                                        },
                                        {
                                            "featureType": "administrative",
                                            "elementType": "geometry.stroke",
                                            "stylers": [{
                                                "color": "#c9b2a6"
                                            }]
                                        },
                                        {
                                            "featureType": "administrative.land_parcel",
                                            "elementType": "geometry.stroke",
                                            "stylers": [{
                                                "color": "#dcd2be"
                                            }]
                                        },
                                        {
                                            "featureType": "administrative.land_parcel",
                                            "elementType": "labels.text.fill",
                                            "stylers": [{
                                                "color": "#ae9e90"
                                            }]
                                        },
                                        {
                                            "featureType": "landscape.natural",
                                            "elementType": "geometry",
                                            "stylers": [{
                                                "color": "#dfd2ae"
                                            }]
                                        },
                                        {
                                            "featureType": "poi",
                                            "elementType": "geometry",
                                            "stylers": [{
                                                "color": "#dfd2ae"
                                            }]
                                        },
                                        {
                                            "featureType": "poi",
                                            "elementType": "labels.text.fill",
                                            "stylers": [{
                                                "color": "#93817c"
                                            }]
                                        },
                                        {
                                            "featureType": "poi.park",
                                            "elementType": "geometry.fill",
                                            "stylers": [{
                                                "color": "#a5b076"
                                            }]
                                        },
                                        {
                                            "featureType": "poi.park",
                                            "elementType": "labels.text.fill",
                                            "stylers": [{
                                                "color": "#447530"
                                            }]
                                        },
                                        {
                                            "featureType": "road",
                                            "elementType": "geometry",
                                            "stylers": [{
                                                "color": "#f5f1e6"
                                            }]
                                        },
                                        {
                                            "featureType": "road.arterial",
                                            "elementType": "geometry",
                                            "stylers": [{
                                                "color": "#fdfcf8"
                                            }]
                                        },
                                        {
                                            "featureType": "road.highway",
                                            "elementType": "geometry",
                                            "stylers": [{
                                                "color": "#f8c967"
                                            }]
                                        },
                                        {
                                            "featureType": "road.highway",
                                            "elementType": "geometry.stroke",
                                            "stylers": [{
                                                "color": "#e9bc62"
                                            }]
                                        },
                                        {
                                            "featureType": "road.highway.controlled_access",
                                            "elementType": "geometry",
                                            "stylers": [{
                                                "color": "#e98d58"
                                            }]
                                        },
                                        {
                                            "featureType": "road.highway.controlled_access",
                                            "elementType": "geometry.stroke",
                                            "stylers": [{
                                                "color": "#db8555"
                                            }]
                                        },
                                        {
                                            "featureType": "road.local",
                                            "elementType": "labels.text.fill",
                                            "stylers": [{
                                                "color": "#806b63"
                                            }]
                                        },
                                        {
                                            "featureType": "transit.line",
                                            "elementType": "geometry",
                                            "stylers": [{
                                                "color": "#dfd2ae"
                                            }]
                                        },
                                        {
                                            "featureType": "transit.line",
                                            "elementType": "labels.text.fill",
                                            "stylers": [{
                                                "color": "#8f7d77"
                                            }]
                                        },
                                        {
                                            "featureType": "transit.line",
                                            "elementType": "labels.text.stroke",
                                            "stylers": [{
                                                "color": "#ebe3cd"
                                            }]
                                        },
                                        {
                                            "featureType": "transit.station",
                                            "elementType": "geometry",
                                            "stylers": [{
                                                "color": "#dfd2ae"
                                            }]
                                        },
                                        {
                                            "featureType": "water",
                                            "elementType": "geometry.fill",
                                            "stylers": [{
                                                "color": "#b9d3c2"
                                            }]
                                        },
                                        {
                                            "featureType": "water",
                                            "elementType": "labels.text.fill",
                                            "stylers": [{
                                                "color": "#92998d"
                                            }]
                                        }
                                    ]
                                }
                            );
                            var marker = new google.maps.Marker({
                                position: location,
                                map: map,
                                icon: "../images/restoran-manjez-beograd-location-pin.svg"
                            });
                            var infoContent =
                                "<h3 class=text-center>Restoran Manjež</h3> <p class=text-center>Svetozara Markovića 49,<br> Beograd 11000,<br> Srbija</p> <a href=#makereservation id=btn-in-location class=btn>Rezervacija</a>";

                            var infoWindow = new google.maps.InfoWindow({
                                content: infoContent
                            });

                            //U slučaju da se zatvori, pokreće se klikom na marker
                            marker.addListener("click", function() {
                                infoWindow.open(map, marker);
                            });
                        }
                    </script>
                    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzd5bEX2qFfPphpBbV3haPx1Vg5SWpkls&callback=initMap"></script>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Contact details and form -->

<section class="contacts py-5">
    <div class="wrapper">
        <h4 class="h2 text-uppercase text-danger text-center"><span class="d-none d-md-inline text-dark">Restoran Manjež Beograd |</span> Kontaktirajte nas</h4>
        <div class="row py-5">
            <a href="" class="col-12 col-sm-4 text-center">
                <img src="../images/restoran-manjez-beograd-email.svg" alt="">
                <p class="pt-3">recepcija@manjez.rs</p>
            </a>
            <a href="" class="col-12 col-sm-4 text-center">
                <img src="../images/restoran-manjez-beograd-telefon.svg" alt="">
                <p class="pt-3">+381 11 362 11 11</p>
            </a>
            <a href="" class="col-12 col-sm-4 text-center">
                <img src="../images/restoran-manjez-beograd-adresa.svg" alt="">
                <p class="pt-3">Svetozara Markovića 49</p>
            </a>
        </div>
        <form class="row w-75 mx-auto" action="">
            <input class="col-12 col-md-5 form-control form-control-lg mb-4" type="name" placeholder="Vaše ime">
            <div class="col-1"></div>
            <input class="col-12 col-md-6 form-control form-control-lg mb-4" type="email" placeholder="Vaš Email">
            <textarea class="col-12 form-control form-control-lg mb-4" name="Message" placeholder="Vaša poruka" id="msg" rows="3"></textarea>
            <button type="Submit" class="btn btn-lg btn-outline-danger w-100">Pošaljite</button>
        </form>

    </div>
</section>

<?php include('inc-footer.php') ?>