<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <title>Restaurant Manjež | Belgrade</title>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Playfair+Display:400,700&amp;subset=latin-ext" rel="stylesheet" />
  <!-- Resources -->
  <link rel="stylesheet" href="css/lightslider.min.css" />
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/style.css" />
  <script src="js/jquery-3.4.1.min.js"></script>


  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png" />
  <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png" />
  <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png" />
  <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png" />
  <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png" />
  <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png" />
  <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png" />
  <link rel="icon" type="image/png" sizes="192x192" href="images/favicon/android-icon-192x192.png" />
  <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png" />
  <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png" />
  <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png" />
  <link rel="manifest" href="images/favicon/manifest.json" />
  <meta name="msapplication-TileColor" content="#ffffff" />
  <meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png" />
  <meta name="theme-color" content="#ffffff" />

  <!-- Open Graph -->

  <meta property="og:title" content="Restaurant Manjez Belgrade" />
  <meta property="og:site_name" content="Restaurant Manjez Belgrade" />
  <meta property="og:url" content="https://manjez.rs" />
  <meta property="og:description" content="Restaurant Manjez is one of the oldest traditional restaurants in Belgrade. Great cuisine, great service, great hotel at low prices." />
  <meta property="og:type" content="restaurant" />
  <meta property="og:image" content="https://manjez.rs/images/restoran-manjez-belgrade-logo.png" />
</head>

<body>
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Restaurant",
      "name": "Restaurant Manjez Belgrade",
      "image": "https://manjez.rs/images/restoran-manjez-beograd-logo.png",
      "@id": "",
      "url": "https://manjez.rs",
      "telephone": "+ 381 11 362 11 11",
      "priceRange": "$",
      "menu": "https://manjez.rs/menu",
      "servesCuisine": "International",
      "acceptsReservations": "true",
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "Svetozara Markovića 49",
        "addressLocality": "Belgrade",
        "postalCode": "11000",
        "addressCountry": "RS"
      },
      "geo": {
        "@type": "GeoCoordinates",
        "latitude": 44.80419864601798,
        "longitude": 20.464742481708527
      },
      "openingHoursSpecification": {
        "@type": "OpeningHoursSpecification",
        "dayOfWeek": [
          "Monday",
          "Tuesday",
          "Wednesday",
          "Thursday",
          "Friday",
          "Saturday",
          "Sunday"
        ],
        "opens": "08:00",
        "closes": "01:00"
      },
      "sameAs": [
        "https://www.instagram.com/manjez_kafana_vila/",
        "https://twitter.com/manjezvilla",
        "https://www.facebook.com/manjez1936/"
      ]
    }
    }
  </script>
  <header>
    <div id="uphead" class="heading-100">
      <div class="wrapper text-center text-md-left">
        <h1 class="">
          Restaurant Manjez | Belgrade | Svetozara Markovica 49
        </h1>
        <ul class="catbtnhead">
          <li class="mr-2">
            <a class="text-white" href="tel:+ 381 11 362 11 11">
              <img src="images/restoran-manjez-beograd-phoneicon.svg" alt="Restaurant Manjez Belgrade" />
              <p>+ 381 11 362 11 11</p>
            </a>
          </li>
          <li>
            <a class="text-white" href=""><img src="images/restoran-manjez-beograd-adressicon.svg" alt="Restaurant Manjez Belgrade" />
              <p>Svetozara Markovića 49</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div id="mainnavwhole" class="header-100">
      <div class="wrapper">
        <a href="index.php" class="logo"><img id="logonav" src="images/restoran-manjez-beograd-logo.png" alt="Restaurant Manjez Belgrade" /></a>

        <nav id="mainnav" class="navbar-expand-md">
          <div class="inmain">
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarNav" id="maincolbutt">
              <img src="images/restoran-manjez-beograd-menu.svg" alt="Restaurant Manjez Belgrade" />
            </button>
            <div class="collapse navbar-collapse text-center" id="navbarNav">
              <ul class="navbar-nav list-unstyled bg-white">
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="index.php"><span>Home</span></a>
                  </h2>
                </li>
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="aboutus.php"><span>About Us</span></a>
                  </h2>
                </li>
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="menu.php"><span>Menu</span></a>
                  </h2>
                </li>
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="#makereservation"><span>Reservation</span></a>
                  </h2>
                </li>
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="gallery.php"><span>Gallery</span></a>
                  </h2>
                </li>
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="contact.php"><span>Contact</span></a>
                  </h2>
                </li>
                <li class="nav-item">
                  <h2>
                    <a class="nav-link" href="sr/index.php"><span>SR</span></a>
                  </h2>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
  </header>